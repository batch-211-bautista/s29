/*
	MongoDB - Query Operators
	Expand Queries in MongoDB using Query Operators
*/

/*
	Overview:

	Query Operator
	- Definition
	- Importance

	Types of Query Operators:
	1. Comparison Query Operators
		1.1 Greater than
		1.2 Greater than or equal to
		1.3 Less than
		1.4 Less than or equal to
		1.5 Not equal to
		1.6 In
	2. Evaluation Query Operators:
		2.1 Regex
			2.1.1 Case sensitive query
			2.1.2 Case insensitive query
	3. Logical Query Operators
		3.1 OR
		3.2 AND

	Field Projection
	1. Inclusion
		1.1 Returning specific fields in embedded documents
		1.2 Exception to the inclusion rule
		1.3 Slice operator
	2. Exclusion
		2.1 Excluding specific fields in embedded documents
*/

/*
	What does Query Operator Mean?
	- A "Query" is a request for data from a database.
	- An Operator is a symbol that represents an action or a process
	- Putting them together, they mean the things that we can do on our queries using certain operators
*/

/*
	Why do we need to study Query Operators?
	- Knowing Query Operators will enable us to create queries that can do more than what simple operators do. (We can do more than what we do in simple CRUD Operations)
	- For example, in our CRUD operations discussion, we have discussed findOne using a specific value inside its single or multiple parameters. When we know query operators, we may look for a records that are more specific.
*/

// 1. COMPARISON QUERY OPERATORS
/*
	Includes:
		- greater than
		- less than
		- greater than or equal to
		- less than or equal to
		- not equal to
		- in
*/

/*
	1.1 GREATER THAN "$gt"
	- "$gt" finds documents that have field numbers that are greater than a specified value.
	- Syntax:
		- db.collectionName.find({field: {$gt: value}});
*/
db.users.find({age: {$gt: 76}});

/*
	1.2 GREATHER THAN OR EQUAL TO: "$gte"
	- "$gte" find documents that have field number values that are greater than or equal to a specific value.
	- Syntax:
		- db.collectionName.find({field: {$gte: value}};
*/
db.users.find({age: {$gte: 76}});

/*
	1.3 LESS THAN OPERATOR: "$lt"
	- "$lt" finds documents that have field number values less than the specified value
	- Syntax:
		- db.collectionName.find({field: {$lt: value}});
*/
db.users.find({age: {$lt: 65}});

/*
	1.4 LESS THAN OR EQUAL TO OPERATOR: "$lte"
	- "$lte" finds documents that have field number values that are less than or equal to a specified value
	- Syntax:
		- db.collectionName.find({field: {$lte: value}});
*/
db.users.find({age: {$lte: 65}});

/*
	1.5 NOT EQUAL TO OPERATOR: "$ne"
	- "$ne" finds documents that have field numbers that are not equal to a specified value
	- Syntax:
		- db.collectionName.find({field: {$ne: value}});
*/
db.users.find({age: {$ne: 65}});

/*
	1.6 IN OPERATOR: "$in"
	- "$in" finds documents with specific match criteria on one field using different values
	- Syntax:
		- db.collectionName.find({field: {$in: value}})
*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});

// 2. EVALUATION QUERY OPERATORS
// - Evaluation operators return data based on evaluations of either individual fields or the entire collection's documents.

/*
	2.1 REGEX OPERATOR: "$regex"
	- RegEx is short for regular expression.
	- They are called regular expressions because they are based on regular languages.
	- Regex is used for matching strings.
	- It allows us to find documents that match a specific string pattern using regular expressions.
*/

/*
	2.1.1 Case sensitive query
	Syntax:
		- db.collectionName.find({field: {$regex: 'pattern'}});
*/
db.users.find({lastName: {$regex: 'A'}});

/*
	2.1.2 Case insensitive query
	- We can run case insensitive queries by utilizing the "i" option
	- Syntax:
		db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}});
*/
db.users.find({lastName: {$regex: 'A', $options: '$i'}});

/*
	MINI-ACTIVITY #1
	Find users with the letter 'e' in their firstname.
	- Make sure to use case insensitive query
	- Screenshot the result from the Robo3t and send on our batch hangouts
	- Pass the results at 6:50
*/
db.users.find({firstName: {$regex: 'e', $options: '$i'}});

// 3. LOGICAL QUERY OPERATORS

/*
	3.1 OR OPERATOR: "$or"
	- "$or" finds documents that match a single criteria from multiple provided search criteria.
	- Syntax:
		- db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}});
*/
db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

/*
	3.2 AND OPERATOR: "$and"
	- "$and" finds documents matching multiple criteria in a single field
	- Syntax:
		- db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]});
*/
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});
/*
	MINI ACTIVITY #3
	- find users with the letter 'e' in their first name and has and age of less than or equal to 30.
*/
db.users.find({$and: [{firstName: {$regex: 'E', $options: '$i'}}, {age: {$lte: 30}}]});

// FIELD PROJECTION
/*
	- By default, MongoDB returns the whole document, especially, when dealing with complex documents.
	- But sometimes, it is not helpful to view the whole document, especially when dealing with complex documents
	- To help with readability of the values returned (or sometime, because of security reasons), we include or exclude some fields.
	- In other words, we project our selected fields.
*/

/*
	1. INCLUSION
		- Allows us to include/add specific fields  only when retrieving documents.
		- The value denoted is "1" to indicate that the field is being included.
		- We cannot do exclusion on field that uses inclusion projection.
		- Syntax:
			- db.collectionName.find({criteria}, {field: 1});
*/
db.users.find({firstName: "Jane"});
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

/*
	1.1 RETURNING SPECIFIC FIELDS IN EMBEDDED DOCUMENTS
	- The double quotations are important.
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

/*
	1.2 EXCEPTION TO THE INCLUSION RULE: Surpressing the ID field
	- Allows us to exclude the "_id" field when retrieving documents
	- When using field projection, field inclusion and exlusion may not be used at the same time.
	- Excluding the "_id" field is the ONLY exception to this rule.
	- Syntax:
		- db.collectionName.find({criteria}, {_id: 0});
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

/*
	1.3 SLICE OPERATOR: "$slice"
	- "$slice" operator allows us to retrieve only 1 element that matches the search criteria.
*/
// To demonstrate, let us first insert and view an array
db.users.insert({
	namearr: [
		{
			namea: "Juan"
		},
		{
			nameb: "Tamad"
		}
	]
});
db.users.find({namearr: {namea: "Juan"}});

// Now, let us use the slice operator
db.users.find(
	{ "namearr": 
		{ 
			namea: "Juan" 
		} 
	}, 
	{ namearr: 
		{ $slice: 1 } 
	}
);

/*
	MINI-ACTIVITY:
	- Find users with letter 's' on their first names. (Use the "i" option in the $regex operator)
	- Show only the firstName and the lastName fields and hide the _id field.
	- Send the screenshots to our batch hangouts.
*/
db.users.find({firstName: {$regex: 'S', $options: '$i'}}, {firstName: 1, lastName: 1, _id: 0});

/*
	2. EXLUSION
	- Allows us to exlude or remove specific fields when retrieving documents
	- The value provided is zero to denote that the field is being included.
	- Syntax:
		- db.collectionName.find({criteria}, {field: 0});
*/
db.users.find(
	{firstName: "Jane"},
	{
		contact: 0,
		department: 0
	}
);

/*
	2.1 EXCLUDING/SURPRESSING SPECIFIC FIELDS IN EMBEDDED DOCUMENT
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0
	}
);