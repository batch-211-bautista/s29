// Find users with letter S in their first name or D in their last name
db.users.find(
	{$or: 
		[
			{firstName: {$regex: "S", $options: "$i"}},
		 	{lastName: {$regex: "D", $options: "$i"}}
		]
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);


// Find users who are from the HR department and their age is greater than or equal to 70
db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});


// Find users with the letter e in their first name and has an age of less than or equal to 30
db.users.find({$and: [{firstName: {$regex: "E", $options: "$i"}}, {age: {$lte: 30}}]});